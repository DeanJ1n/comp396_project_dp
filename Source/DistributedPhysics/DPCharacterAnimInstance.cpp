// Fill out your copyright notice in the Description page of Project Settings.


#include "DPCharacterAnimInstance.h"
#include "DistributedPhysics/DistributedPhysicsCharacter.h"

void UDPCharacterAnimInstance::UpdateAnimationInfo()
{
	// update required info on animation update
	ADistributedPhysicsCharacter* tempCharacter = Cast<ADistributedPhysicsCharacter>(GetOwningActor());
	if (!tempCharacter)
	{
		return;
	}
	bIsJumping = tempCharacter->bIsJumping;
	bIsCrouching = tempCharacter->bIsCrouched;
	const FVector velocity = tempCharacter->GetVelocity();
	const FRotator rotation = tempCharacter->GetActorRotation();
	direction = CalculateDirection(velocity, rotation);
	speed = velocity.Size();
}
