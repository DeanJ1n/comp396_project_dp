// Fill out your copyright notice in the Description page of Project Settings.


#include "DPRewindGameMode.h"
#include "PhysicsTestActorBase.h"
#include "EngineUtils.h"
#include "Engine/World.h"

ADPRewindGameMode::ADPRewindGameMode()
{
	bIsRewindMode = true;
}

void ADPRewindGameMode::BeginPlay()
{
	UWorld* world = GetWorld();
	for (TActorIterator<APhysicsTestActorBase> it(world, APhysicsTestActorBase::StaticClass()); it; ++it)
	{
		if ((*it)->bRecordPosition)
		{
			rewindObjectCount++;
		}
	}
	
	UE_LOG(LogTemp, Warning, TEXT("Rewind object count is: %d"), rewindObjectCount);
	Super::BeginPlay();
}

void ADPRewindGameMode::RewindReady()
{
	rewindReadyCount++;
	UE_LOG(LogTemp, Warning, TEXT("%d out of %d objetcts are ready for rewinding"), rewindReadyCount, rewindObjectCount);
	if (rewindReadyCount != rewindObjectCount)
	{
		return;
	}
	// rewinding start when all objects are ready for rewinding
	FDateTime earliest = FDateTime::UtcNow();
	UWorld* world = GetWorld();
	UE_LOG(LogTemp, Warning, TEXT("ready to rewind"));
	// find the earliest time stamp to calculate timer offset
	for (TActorIterator<APhysicsTestActorBase> it(world, APhysicsTestActorBase::StaticClass()); it; ++it)
	{
		for (const FLocationInfoPointSet& f : (*it)->machinesPointSets)
		{
			if (f.points.Num() <= 0)
			{
				continue;
			}
			if (f.points[0].time < earliest)
			{
				earliest = f.points[0].time;
			}
		}
	}
	
	// initialize all rewind rendering on objects
	for (TActorIterator<APhysicsTestActorBase> it(world, APhysicsTestActorBase::StaticClass()); it; ++it)
	{
		for (int32 i = 0; i < (*it)->machinesPointSets.Num(); i++)
		{
			FLocationInfoPointSet& f = (*it)->machinesPointSets[i];
			if (f.points.Num() <= 0)
			{
				continue;
			}
			(*it)->InitializeRewindRender(i, (f.points[0].time - earliest).GetTotalSeconds() + rewindDelay);
		}
	}
}
