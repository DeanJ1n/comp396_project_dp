// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "DPCharacterAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class DISTRIBUTEDPHYSICS_API UDPCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

protected:
	// infos required to update character animation
	UPROPERTY(BlueprintReadWrite, Category = "Animation Update")
	bool bIsJumping = false;
	UPROPERTY(BlueprintReadWrite, Category = "Animation Update")
	bool bIsCrouching = false;
	UPROPERTY(BlueprintReadWrite, Category = "Animation Update")
	float direction = 0.0f;
	UPROPERTY(BlueprintReadWrite, Category = "Animation Update")
	float speed = 0.0f;

protected:
	UFUNCTION(BlueprintCallable, Category = "Animation Update")
	virtual void UpdateAnimationInfo();
	
};
