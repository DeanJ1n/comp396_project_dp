// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Net/UnrealNetwork.h"
#include "PhysicsTestActorBase.generated.h"

class ADPPlayerController;
struct FTimerHandle;
struct FColor;
const int32 SERVER_ID = -500;
const int32 ERROR_CODE = -1;

// for drawing debug lines
struct FSynchronizationDebugInfo
{
	int32 machinID = -1;
	FVector position;

	FSynchronizationDebugInfo() : machinID(-1), position(FVector::ZeroVector){}
};

// for storing location of the object at a specific time stamp
struct FTimeStampLocationInfo
{
	FDateTime time;
	FVector position;

	FTimeStampLocationInfo(){}

public:
	static int32 GetPointFromString(FTimeStampLocationInfo& outPoint, const FString& infoString)
	{
		TArray<FString> temp;
		infoString.ParseIntoArray(temp, TEXT(","), true);
		if (temp.Num() < 3)
		{
			return ERROR_CODE;
		}
		// parse the location vector
		TArray<FString> locationText;
		temp[1].ParseIntoArrayWS(locationText, (const TCHAR*) nullptr, true);
		outPoint.position = FVector(
			FCString::Atof(*(locationText[0].RightChop(2))),
			FCString::Atof(*(locationText[1].RightChop(2))),
			FCString::Atof(*(locationText[2].RightChop(2)))
		);
		TArray<FString> dateText;
		temp[2].ParseIntoArray(dateText, TEXT("-"), true);
		TArray<FString> dateDetails;
		TArray<FString> timeDetails;
		dateText[0].ParseIntoArray(dateDetails, TEXT("."), true);
		dateText[1].ParseIntoArray(timeDetails, TEXT("."), true);
		outPoint.time = FDateTime(
			FCString::Atoi(*dateDetails[0]),
			FCString::Atoi(*dateDetails[1]),
			FCString::Atoi(*dateDetails[2]),
			FCString::Atoi(*timeDetails[0]),
			FCString::Atoi(*timeDetails[1]),
			FCString::Atoi(*timeDetails[2]),
			FCString::Atoi(*timeDetails[3])
		);
		return FCString::Atoi(*temp[0]);
	}

	FTimeStampLocationInfo(FDateTime inTime, FVector inPosition) : time(inTime), position(inPosition){}
	FString ToString() const
	{
		return "(" + time.ToString() + "." + FString::FromInt(time.GetMillisecond()) + ") " + position.ToString() ;
	}
	FString ToRewindData() const
	{
		return position.ToString() + "," + time.ToString() + "." + FString::FromInt(time.GetMillisecond());
	}
};

// storing all points at all time stamps
struct FLocationInfoPointSet
{
	int32 machineID = -1;
	FString machineIDText = "";
	TArray<FTimeStampLocationInfo> points;

public:
	FLocationInfoPointSet(int32 inID) : machineID(inID)
	{
		if (inID == SERVER_ID)
		{
			machineIDText = "server";
		}
		else
		{
			machineIDText = "client " + FString::FromInt(inID);
		}
	}
	FLocationInfoPointSet(){}
	FString ToString() const
	{
		FString temp = machineIDText + "\n";
		for (const FTimeStampLocationInfo& p : points)
		{
			temp += (p.ToString() + "\n");
		}
		return temp;
	}
	FString ToRewindData() const
	{
		FString temp = "";
		for (const FTimeStampLocationInfo& p : points)
		{
			temp += (FString::FromInt(machineID) + "," + p.ToRewindData() + LINE_TERMINATOR_ANSI);
		}
		return temp;
	}
	static float FrechetHelper(const TArray<FTimeStampLocationInfo>& f, const TArray<FTimeStampLocationInfo>& g, TArray<float>& dists, int32 i, int32 j, int32 fSize, int32 gSize)
	{
		int32 curIndex = gSize * i + j;
		// when the number is set already, just return it
		if (curIndex >= dists.Num() || curIndex < 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Cur index at %d of i = %d, j = %d is not valid"), curIndex, i, j);
			return 0;
		}
		if (i >= f.Num() || i < 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Invalid i of %d for f has %d elements"), i, f.Num());
			return 0;
		}
		if (j >= g.Num() || j < 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Invalid j of %d for g has %d elements"), j, g.Num());
			return 0;
		}
		if (dists[curIndex] > -1.0f)
		{
			return dists[curIndex];
		}
		// base cases
		if (i == 0 && j == 0)
		{
			dists[curIndex] = FVector::Distance(f[i].position, g[j].position);
		}
		else if (i > 0 && j == 0)
		{
			dists[curIndex] = FMath::Max<float>(FrechetHelper(f, g, dists, i - 1, 0, fSize, gSize), FVector::Distance(f[i].position, g[0].position));
		}
		else if (i == 0 && j > 0)
		{
			dists[curIndex] = FMath::Max<float>(FrechetHelper(f, g, dists, 0, j - 1, fSize, gSize), FVector::Distance(f[0].position, g[j].position));
		}
		else if (i > 0 && j > 0)
		{
			dists[curIndex] = FMath::Max<float>(
				FMath::Min3<float>(
				FrechetHelper(f, g, dists, i - 1, j, fSize, gSize), 
				FrechetHelper(f, g, dists, i - 1, j - 1, fSize, gSize), 
				FrechetHelper(f, g, dists, i, j - 1, fSize, gSize)), 
				FVector::Distance(f[i].position, g[j].position));
		}
		return dists[curIndex];
	}
	static float FrechetDistance(const FLocationInfoPointSet& f, const FLocationInfoPointSet& g)
	{
		// get important info to do the DP
		TArray<float> dists;
		const int32 fSize = f.points.Num();
		const int32 gSize = g.points.Num();
		UE_LOG(LogTemp, Log, TEXT("function f has %d points, function g has %d points"), fSize, gSize);
		dists.Init(-1.0f, fSize * gSize);
		return FrechetHelper(f.points, g.points, dists, fSize - 1, gSize - 1, fSize, gSize);
	}
};

/**
 * 
 */
UCLASS()
class DISTRIBUTEDPHYSICS_API APhysicsTestActorBase : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	APhysicsTestActorBase();
	virtual void BeginPlay() override;
	virtual void Tick(float deltaTime) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void AssignNetOwner(ADPPlayerController* pc);
	/** activate simulation on local machine then broad cast to server and other clients, should be called by owner */
	virtual void LocalActivateSimulation(const bool bDoActivate = true);	
	virtual void TickConvergence(float deltaTime);
	void RenderRewindData(int32 index, int32 id);
	void InitializeRewindRender(int32 id, float delay);

	// UFUNCTIONS
	UFUNCTION(BlueprintImplementableEvent, Category = "Simulation Funcrions")
	void OnSimulationEnabled(bool bDoEnable = true);

	// RPCs
	UFUNCTION(NetMulticast, Reliable)
	void MulticastExchangeLocationInfo(const int32 machineID, const FVector curLoc);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerExchangeLocationInfo(const int32 machineID, const FVector curLoc, const FDateTime curTime);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastStartSimulation(const bool bDoStart = true);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStartSimulation(const bool bDoStart = true);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSyncMovement(const FVector curLoc, const FVector curVelocity, const FDateTime curTime);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSyncMovement(const FVector curLoc, const FVector curVelocity, const FDateTime curTime);
	

protected:
	void LogInfo();
	void LogInfo(const int32 machineID, const FVector curLoc, const FDateTime curTime);
	/** Draws debug info for only local machine */
	void DrawDebugInfo();
	/** Function for clients to exchange their actor location info across server */
	void ExchangeLocationInfo();
	void DrawHelper(const FVector prevLoc, const FVector curLoc, const FColor drawColor, bool bDoDrawLine);
	void EnableSimulation();
	void AssignObjectClientID();
	virtual void SyncMovement();

	// RPCs
	UFUNCTION()
	void OnRep_EnableSimulation();

public:
	UPROPERTY(EditAnywhere, Category = "Information viewing")
	bool bRecordPosition = true;

protected:
	UPROPERTY(Replicated, EditAnywhere, Category = "Information viewing")
	bool bDoLogPosition = false;
	UPROPERTY(EditAnywhere, Category = "Information viewing")
	bool bLogToScreen = false;
	UPROPERTY(EditAnywhere, Category = "Information viewing")
	float messageLogDelay = 1.0f;
	UPROPERTY(EditAnywhere, Category = "Information viewing")
	float locationInfoSyncPeriod = 0.1f;
	UPROPERTY(EditAnywhere, Category = "Information viewing")
	TArray<FColor> clientDebugColors;	// colors for drawing debug info
	UPROPERTY(EditAnywhere, Category = "Information viewing")
	FColor serverDebugColor;	// colors for drawing debug info

	UPROPERTY(EditAnywhere, Category = "Debug Info Specification")
	float debugPointSize = 3.6f;
	UPROPERTY(EditAnywhere, Category = "Debug Info Specification")
	float debugGraphicLife = 3.6f;
	UPROPERTY(EditAnywhere, Category = "Debug Info Specification")
	bool bDrawDebugLine = true;
	UPROPERTY(EditAnywhere, Category = "Debug Info Specification")
	bool bDoPresentServer = true;
	UPROPERTY(EditAnywhere, Category = "Debug Info Specification")
	int32 assumedClientNumber = 4;
	UPROPERTY(EditAnywhere, Category = "Debug Info Specification")
	float simulationStartDelay = 1.0f;

	// below are for location syncing
	UPROPERTY(EditAnywhere, Category = "Movement Syncing")
	float movementSyncPeriod = 0.1f;
	UPROPERTY(EditAnywhere, Category = "Movement Syncing")
	float targetSwapingperiod = 1.0f;
	UPROPERTY(EditAnywhere, Category = "Movement Syncing")
	float interpolationSpeed = 2.5f;
	bool bIsMovementSyncingOn = false;
	FVector targetLocation;
	FVector targetVelocity;
	FDateTime lastTargetTime;
	FTimerHandle movementSyncTimer;


	// simulation required fields
	UPROPERTY(ReplicatedUsing = OnRep_EnableSimulation, VisibleAnyWhere, Category = "Sync Properties")
	bool bIsSimulationOn = false;	// used for controlling remote simulation
	UPROPERTY(Replicated, VisibleAnyWhere, Category = "Sync Properties")
	int32 netOwnerID = -1;	// used for controlling simulation
	bool bIsLocalSimulationOn = false;	// used for controlling local simulation

#if WITH_EDITOR
	// below are for position data rewinding 
	bool bIsRewinding = false;
#endif

	// TODO some timers might not be needed here
	FTimerHandle simulationDelayTimer;
	FTimerHandle logTimer;	// timer for printing and drawing debug info
	FTimerHandle locationInfoSyncTimer;	// timer for printing and drawing debug info
	FVector serverPrevLocation;
	FVector prevLocalPosition;

public:
	FString machineIDText = "";	// machine ID related to this specific actor
	int32 localMachineID = -1;
	TArray<FTimerHandle> rewindingTimers;
	TArray<FSynchronizationDebugInfo> machinesLocationInfo;	// location info for all client machines
	TArray<FLocationInfoPointSet> machinesPointSets;	// point sets for all machines for curve comparison

// below are editor specific members
#if WITH_EDITOR
protected:
	bool bDoSaveToTestSummary = false;	// check indicator to decide if to save compute result to test summary
#endif

};
