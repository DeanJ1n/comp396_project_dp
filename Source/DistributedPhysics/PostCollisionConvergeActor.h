// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PhysicsTestActorBase.h"
#include "PostCollisionConvergeActor.generated.h"

struct FHitResult;

/**
 * 
 */
UCLASS()
class DISTRIBUTEDPHYSICS_API APostCollisionConvergeActor : public APhysicsTestActorBase
{
	GENERATED_BODY()

public:
	APostCollisionConvergeActor();

	virtual void LocalActivateSimulation(const bool bDoActivate /* = true */);
	virtual void TickConvergence(float deltaTime);
	virtual void SyncMovement();

	UFUNCTION()
		void OnHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit);

protected:
	// TODO a more flexible way might be required to do make a smoother convergence
	bool bIsAfterHit = false;
	
};
