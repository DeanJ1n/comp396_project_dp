// Fill out your copyright notice in the Description page of Project Settings.


#include "ConstantConvergeActor.h"
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"

AConstantConvergeActor::AConstantConvergeActor()
{

}

void AConstantConvergeActor::LocalActivateSimulation(const bool bDoActivate /* = true */)
{
	Super::LocalActivateSimulation(bDoActivate);
	if (bDoActivate && netOwnerID == localMachineID)
	{
		// activate syncing of movement
		UWorld* world = GetWorld();
		if (!world)
		{
			return;
		}
		world->GetTimerManager().SetTimer(movementSyncTimer, this, &AConstantConvergeActor::SyncMovement, movementSyncPeriod, true);
	}
}

void AConstantConvergeActor::TickConvergence(float deltaTime)
{
	Super::TickConvergence(deltaTime);
	if (bIsMovementSyncingOn && (netOwnerID != localMachineID))
	{
		FVector intendedLocation = FMath::VInterpTo(GetActorLocation(), targetLocation, deltaTime, interpolationSpeed);
		SetActorLocation(intendedLocation, false, nullptr, ETeleportType::TeleportPhysics);
		// for remote machine, interpolate location and velocity
		UStaticMeshComponent* comp = GetStaticMeshComponent();
		if (!comp)
		{
			return;
		}
		FVector intendedVelocity = FMath::VInterpTo(comp->GetPhysicsLinearVelocity(), targetVelocity, deltaTime, interpolationSpeed);
		comp->SetPhysicsLinearVelocity(intendedVelocity);
	}
}

void AConstantConvergeActor::SyncMovement()
{
	Super::SyncMovement();
}
