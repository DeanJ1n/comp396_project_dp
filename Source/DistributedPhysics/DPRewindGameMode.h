// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DistributedPhysicsGameMode.h"
#include "DPRewindGameMode.generated.h"

/**
 * 
 */
UCLASS()
class DISTRIBUTEDPHYSICS_API ADPRewindGameMode : public ADistributedPhysicsGameMode
{
	GENERATED_BODY()

public:
	ADPRewindGameMode();
	virtual void BeginPlay() override;
	void RewindReady();

	int32 rewindObjectCount = 0;
	int32 rewindReadyCount = 0;

	UPROPERTY(EditAnywhere, Category = "Information Viewing")
	float rewindDelay = 3.0f;

protected:
	TArray<UClass*> dpActorSubClasses;

};
