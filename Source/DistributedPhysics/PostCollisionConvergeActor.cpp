// Fill out your copyright notice in the Description page of Project Settings.


#include "PostCollisionConvergeActor.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"

APostCollisionConvergeActor::APostCollisionConvergeActor()
{
	UStaticMeshComponent* mesh = GetStaticMeshComponent();
	if (mesh)
	{
		mesh->OnComponentHit.AddDynamic(this, &APostCollisionConvergeActor::OnHit);
	}
}

void APostCollisionConvergeActor::LocalActivateSimulation(const bool bDoActivate /* = true */)
{
	Super::LocalActivateSimulation(bDoActivate);
}

void APostCollisionConvergeActor::TickConvergence(float deltaTime)
{
	Super::TickConvergence(deltaTime);
	if (bIsMovementSyncingOn && (netOwnerID != localMachineID) && bIsAfterHit)
	{
		FVector intendedLocation = FMath::VInterpTo(GetActorLocation(), targetLocation, deltaTime, interpolationSpeed);
		SetActorLocation(intendedLocation, false, nullptr, ETeleportType::TeleportPhysics);
		// for remote machine, interpolate location and velocity
		UStaticMeshComponent* comp = GetStaticMeshComponent();
		if (!comp)
		{
			return;
		}
		FVector intendedVelocity = FMath::VInterpTo(comp->GetPhysicsLinearVelocity(), targetVelocity, deltaTime, interpolationSpeed);
		comp->SetPhysicsLinearVelocity(intendedVelocity);
	}
}

void APostCollisionConvergeActor::SyncMovement()
{
	Super::SyncMovement();
}

void APostCollisionConvergeActor::OnHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	if (Cast<APhysicsTestActorBase>(otherActor))
	{
		bIsAfterHit = true;
		UE_LOG(LogTemp, Warning, TEXT("After hit"));
	}
	else 
	{
		return;
	}
	if (bIsAfterHit && netOwnerID == localMachineID)
	{
		UE_LOG(LogTemp, Warning, TEXT("Convergence start"));
		// activate syncing of movement
		UWorld* world = GetWorld();
		if (!world)
		{
			return;
		}
		world->GetTimerManager().SetTimer(movementSyncTimer, this, &APostCollisionConvergeActor::SyncMovement, movementSyncPeriod, true);
	}
}
