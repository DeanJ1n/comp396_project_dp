// Fill out your copyright notice in the Description page of Project Settings.


#include "DPPlayerController.h"
#include "PhysicsTestActorBase.h"

ADPPlayerController::ADPPlayerController()
{

}

void ADPPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ADPPlayerController, clientID);
}

void ADPPlayerController::ServerSyncDPActorLocationInfo_Implementation(APhysicsTestActorBase* testActor, const int32 machineID, const FVector curLoc, const FDateTime curTime)
{
	if (!testActor)
	{
		return;
	}
	testActor->ServerExchangeLocationInfo(machineID, curLoc, curTime);
}

bool ADPPlayerController::ServerSyncDPActorLocationInfo_Validate(APhysicsTestActorBase* testActor, const int32 machineID, const FVector curLoc, const FDateTime curTime)
{
	return true;
}
