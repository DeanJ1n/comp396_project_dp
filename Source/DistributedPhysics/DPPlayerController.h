// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Net/UnrealNetwork.h"
#include "DPPlayerController.generated.h"

class APhysicsTestActorBase;
struct FDateTime;

/**
 * 
 */
UCLASS()
class DISTRIBUTEDPHYSICS_API ADPPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ADPPlayerController();
	UPROPERTY(BlueprintReadWrite, Replicated, Category = "Distributed Info")
	int32 clientID = -1;	// if the id is -1, it indicates the client has not yet been assigned an ID
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

	// Sync Functions
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSyncDPActorLocationInfo(APhysicsTestActorBase* testActor, const int32 machineID, const FVector curLoc, const FDateTime curTime);

};
