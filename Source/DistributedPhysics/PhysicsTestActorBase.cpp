// Fill out your copyright notice in the Description page of Project Settings.


#include "PhysicsTestActorBase.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "DPPlayerController.h"
#include "Misc/DateTime.h"
#include "DistributedPhysicsGameMode.h"
#include "TimerManager.h"
#include "Components/StaticMeshComponent.h"

// if with editor we need following libraries for testing purpose
#if WITH_EDITOR
#include "HAL/PlatformFilemanager.h"
#include "HAL/PlatformFile.h"
#include "Misc/FileHelper.h"
#include "DPRewindGameMode.h"
const FString TEST_DIR = FPaths::ProjectDir() + "AutoTests/";
const FString REWIND_DIR = FPaths::ProjectDir() + "RewindData/";
const FString INDICATOR = "indicator.txt";
const FString SUFFIX = ".csv";
#endif


APhysicsTestActorBase::APhysicsTestActorBase()
{
	// replicate the actor across clients
	bReplicates = true;
	serverDebugColor = FColor::Red;
	UStaticMeshComponent* mesh = GetStaticMeshComponent();
	if (mesh)
	{
		mesh->SetNotifyRigidBodyCollision(true);
	}
}

void APhysicsTestActorBase::BeginPlay()
{
	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}
#if WITH_EDITOR
	// check for indicator to decide whether to save compute result to test summary
	IPlatformFile& platFiles = FPlatformFileManager::Get().GetPlatformFile();
	bDoSaveToTestSummary = platFiles.FileExists(*(TEST_DIR + INDICATOR));
	// check if currently it is rewind mode
	ADPRewindGameMode* rewindGameMode =  Cast<ADPRewindGameMode>(world->GetAuthGameMode());
	if (rewindGameMode && bRecordPosition)
	{
		bIsRewinding = rewindGameMode->bIsRewindMode;
	}
#endif
	Super::BeginPlay();
	prevLocalPosition = GetActorLocation();
	serverPrevLocation = GetActorLocation();
	// TODO can make it depend on choice of presenting server or not
	machinesLocationInfo.SetNum(assumedClientNumber + 1);
	// initialize debug info array
	for (int32 i = 0; i < machinesLocationInfo.Num(); i++)
	{
		machinesLocationInfo[i] = FSynchronizationDebugInfo();
	}

	// assign machine ID
	if (HasAuthority())
	{
		localMachineID = SERVER_ID;
		UE_LOG(LogTemp, Log, TEXT("This message is from server"));
		machineIDText = "Server";
		// if its server, it will store all the curves for curve comparison
		machinesPointSets.SetNum(assumedClientNumber + 1);
		for (int32 i = 0; i < assumedClientNumber; i++)
		{
			machinesPointSets[i] = FLocationInfoPointSet(i);
		}
		// again let the last index to be the server
		machinesPointSets[assumedClientNumber] = FLocationInfoPointSet(SERVER_ID);

#if WITH_EDITOR
		// if we are currently rewinding, ignore further operations and start rendering rewind result
		if (bIsRewinding)
		{
			FString rewindFileName = REWIND_DIR + GetWorld()->GetName() + "/";
			rewindFileName += (UKismetSystemLibrary::GetDisplayName(this) + SUFFIX);
			// if the rewinding data file does not exist, return
			if (!platFiles.FileExists(*rewindFileName))
			{
				UE_LOG(LogTemp, Warning, TEXT("Rewinding data missing!"));
				return;
			}
			FString rewindData = "";
			FFileHelper::LoadFileToString(rewindData, *rewindFileName);
			TArray<FString> tempInfo;
			rewindData.ParseIntoArrayLines(tempInfo, true);
			// store parsed data into points for rewinding to work
			for (FString& s : tempInfo)
			{
				FTimeStampLocationInfo tempLocationInfo;
				int index = FTimeStampLocationInfo::GetPointFromString(tempLocationInfo, s);
				machinesPointSets[(index == SERVER_ID) ? assumedClientNumber : index].points.Add(tempLocationInfo);
				// TODO if evidence indicates the timestamp point could be wrong, sort it then do other procedures
			}
			// when rewinding initialize rewind timers
			for (int32 i = 0; i < assumedClientNumber; i++)
			{
				rewindingTimers.Add(FTimerHandle());
			}
			UE_LOG(LogTemp, Warning, TEXT("Rewinding file loaded on: %s"), *UKismetSystemLibrary::GetDisplayName(this));
			rewindGameMode->RewindReady();
			return;
		}
#endif
		return;
	}
	AssignObjectClientID();
	// set up timer to ensure all simulation starts simultaneously
	world->GetTimerManager().SetTimer(simulationDelayTimer, this, &APhysicsTestActorBase::EnableSimulation, simulationStartDelay, false);
}

void APhysicsTestActorBase::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	// call tick converge function
	TickConvergence(deltaTime);
}

void APhysicsTestActorBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// clear all timers
	UWorld* world = GetWorld();
	world->GetTimerManager().ClearTimer(logTimer);
	world->GetTimerManager().ClearTimer(locationInfoSyncTimer);
	world->GetTimerManager().ClearTimer(movementSyncTimer);
	for (FTimerHandle& timer : rewindingTimers)
	{
		world->GetTimerManager().ClearTimer(timer);
	}
	if (localMachineID == SERVER_ID && bDoLogPosition)
	{
		// when ending play, stop simulation
		bIsSimulationOn = false;
		bIsLocalSimulationOn = false;
#if WITH_EDITOR
		if (bIsRewinding)
		{
			return;
		}
		// when saving result to summary following code are required
		IPlatformFile& platFiles = FPlatformFileManager::Get().GetPlatformFile();
		FString resultSummary = "";
		FString fileName = "";
		if (bDoSaveToTestSummary)
		{
			fileName = TEST_DIR + GetWorld()->GetName() + "/";
			if (!platFiles.DirectoryExists(*fileName))
			{
				platFiles.CreateDirectory(*fileName);
			}
			// check for corresponding file, if not exist create one and make titles
			fileName += (UKismetSystemLibrary::GetDisplayName(this) + SUFFIX);
			if (!platFiles.FileExists(*fileName))
			{
				const int32 bound = bDoPresentServer ? machinesPointSets.Num() : assumedClientNumber;
				for (int32 i = 0; i < bound; i++)
				{
					for (int32 j = i + 1; j < bound; j++)
					{
						resultSummary += (
							"\"" +
							machinesPointSets[i].machineIDText +
							" - " +
							machinesPointSets[j].machineIDText +
							"\"" +
							(((i == (bound - 2)) && (j == (bound - 1))) ? LINE_TERMINATOR_ANSI : ","));
					}
				}
			}
			else
			{
				FFileHelper::LoadFileToString(resultSummary, *fileName);
			}
		}
		// when saving  position to rewind data is required, parse and save the position data of this simulation
		if (bRecordPosition)
		{
			FString rewindFileName = REWIND_DIR + GetWorld()->GetName() + "/";
			if (!platFiles.DirectoryExists(*rewindFileName))
			{
				platFiles.CreateDirectory(*rewindFileName);
			}
			rewindFileName += (UKismetSystemLibrary::GetDisplayName(this) + SUFFIX);
			FString rewindData = "";
			const int32 bound = bDoPresentServer ? machinesPointSets.Num() : assumedClientNumber;
			for (int32 i = 0; i < bound; i++)
			{
				rewindData += machinesPointSets[i].ToRewindData();
			}
			FFileHelper::SaveStringToFile(rewindData, *rewindFileName);
		}
#endif
		// calculate all frechet distance between each curves
		const int32 bound = bDoPresentServer ? machinesPointSets.Num() : assumedClientNumber;
		for (int32 i = 0; i < bound; i++)
		{
			for (int32 j = i + 1; j < bound; j++)
			{
				UE_LOG(LogTemp, Log, TEXT("Calculate Frechet distance between %s and %s"), *(machinesPointSets[i].machineIDText), *(machinesPointSets[j].machineIDText));
				const float dist = FLocationInfoPointSet::FrechetDistance(machinesPointSets[i], machinesPointSets[j]);
				UE_LOG(LogTemp, Log, TEXT("[Object: %s] - Frechet Distance between [%s] and [%s] is: %f"),
					*(UKismetSystemLibrary::GetDisplayName(this)),
					*(machinesPointSets[i].machineIDText),
					*(machinesPointSets[j].machineIDText),
					dist);
#if WITH_EDITOR
				if (bDoSaveToTestSummary)
				{
					// when do record test summary, the frechet distance will be recorded
					resultSummary += (FString::SanitizeFloat(dist) + (((i == (bound - 2)) && (j == (bound - 1))) ? LINE_TERMINATOR_ANSI : ","));
				}
#endif
			}
		}
		FFileHelper::SaveStringToFile(resultSummary, *fileName);
	}
	Super::EndPlay(EndPlayReason);
}

void APhysicsTestActorBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APhysicsTestActorBase, bDoLogPosition);
	DOREPLIFETIME(APhysicsTestActorBase, bIsSimulationOn);
	DOREPLIFETIME(APhysicsTestActorBase, netOwnerID);
}

void APhysicsTestActorBase::AssignNetOwner(ADPPlayerController* pc)
{
	// set the net owner of the actor as well as record the netowner ID
	SetOwner(pc);
	netOwnerID = pc->clientID;
}

void APhysicsTestActorBase::LocalActivateSimulation(const bool bDoActivate /*= true*/)
{
	// do activate on local machine if local machine is the net owner of the object
	if (netOwnerID == localMachineID)
	{
		UE_LOG(LogTemp, Warning, TEXT("Client %d as the owner of %s will start simulation."), localMachineID, *(UKismetSystemLibrary::GetDisplayName(this)));
		MulticastStartSimulation(bDoActivate);
		ServerStartSimulation(bDoActivate);
	}
}

void APhysicsTestActorBase::TickConvergence(float deltaTime)
{
	// since its the naive approach, do nothing
}

void APhysicsTestActorBase::RenderRewindData(int32 index, int32 id)
{
	if (index >= machinesPointSets[id].points.Num())
	{
		return;
	}
	FColor drawColor = (id == assumedClientNumber) ? serverDebugColor : clientDebugColors[id];
	FTimeStampLocationInfo prevPoint = machinesPointSets[id].points[(index == 0) ? 0 : index - 1];
	FTimeStampLocationInfo curPoint = machinesPointSets[id].points[index];
	DrawHelper(prevPoint.position, curPoint.position, drawColor, bDrawDebugLine);
	// set timer for next rendering
	if (index + 1 >= machinesPointSets[id].points.Num())
	{
		return;
	}
	FTimeStampLocationInfo nextPoint = machinesPointSets[id].points[index + 1];
	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}
	FTimerDelegate tempDelegate = FTimerDelegate::CreateUObject(this, &APhysicsTestActorBase::RenderRewindData, index + 1, id);
	world->GetTimerManager().SetTimer(rewindingTimers[id], tempDelegate, (nextPoint.time - curPoint.time).GetTotalSeconds(), false);
}

void APhysicsTestActorBase::InitializeRewindRender(int32 id, float delay)
{
	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}
	FTimerDelegate tempDelegate = FTimerDelegate::CreateUObject(this, &APhysicsTestActorBase::RenderRewindData, 0, id);
	world->GetTimerManager().SetTimer(rewindingTimers[id], tempDelegate, delay, false);
	UE_LOG(LogTemp, Warning, TEXT("Initilizing rewind rendering on: %s, of machine id: %d"), *UKismetSystemLibrary::GetDisplayName(this), id);
}

void APhysicsTestActorBase::MulticastSyncMovement_Implementation(const FVector curLoc, const FVector curVelocity, const FDateTime curTime)
{
	// when received movement info, if this is netowner ignore
	if(netOwnerID == localMachineID)
	{
		return;
	}
	// when first received syncing info, turn on syncing flag
	FDateTime localCurTime = FDateTime::UtcNow();
	if (!bIsMovementSyncingOn)
	{
		lastTargetTime = localCurTime;
		bIsMovementSyncingOn = true;
	}
	// use local current time to check how long remain for reaching the target
	float timeSpan = (float)(lastTargetTime - curTime).GetTotalSeconds() + movementSyncPeriod;
	if (timeSpan < 0)
	{
		lastTargetTime = localCurTime;
		timeSpan = (float)(lastTargetTime - curTime).GetTotalSeconds() + movementSyncPeriod;
	}
	targetVelocity = curVelocity;
	targetLocation = curVelocity * timeSpan + curLoc;
}

void APhysicsTestActorBase::ServerSyncMovement_Implementation(const FVector curLoc, const FVector curVelocity, const FDateTime curTime)
{
	MulticastSyncMovement(curLoc, curVelocity, curTime);
}

bool APhysicsTestActorBase::ServerSyncMovement_Validate(const FVector curLoc, const FVector curVelocity, const FDateTime curTime)
{
	return true;
}

void APhysicsTestActorBase::ServerStartSimulation_Implementation(const bool bDoStart /*= true*/)
{
	MulticastStartSimulation(bDoStart);
}

bool APhysicsTestActorBase::ServerStartSimulation_Validate(const bool bDoStart /*= true*/)
{
	return true;
}

void APhysicsTestActorBase::MulticastStartSimulation_Implementation(const bool bDoStart /*= true*/)
{
	// activate simulation and start physics simulations
	bIsSimulationOn = bDoStart;
	// if local simulation is set to remote simulation, ignore following operations
	if (bIsLocalSimulationOn == bIsSimulationOn)
	{
		return;
	}
	else
	{
		bIsLocalSimulationOn = bIsSimulationOn;
	}
	UStaticMeshComponent* tempMesh = GetStaticMeshComponent();
	if (!tempMesh)
	{
		return;
	}
	tempMesh->SetSimulatePhysics(bDoStart);
	if (bDoStart)
	{
		// Set up info logging timers
		UWorld* world = GetWorld();
		if (!world)
		{
			return;
		}
		// world->GetTimerManager().SetTimer(logTimer, this, &APhysicsTestActorBase::LogInfo, messageLogDelay, true);
		world->GetTimerManager().SetTimer(locationInfoSyncTimer, this, &APhysicsTestActorBase::ExchangeLocationInfo, locationInfoSyncPeriod, true);
	}
	OnSimulationEnabled(bDoStart);
}

void APhysicsTestActorBase::LogInfo()
{
	if (!bDoLogPosition)
	{
		return;
	}
	FString tempString = "[" + machineIDText + "]Position of: " + UKismetSystemLibrary::GetDisplayName(this) + " is: " + GetActorLocation().ToString();
	// also record the time when this is logged to the screen
	FDateTime curTime = FDateTime::UtcNow();
	FString curTimeString = "   ---   " + curTime.ToString() + "." + FString::FromInt(curTime.GetMillisecond());
	tempString += curTimeString;
	UE_LOG(LogTemp, Log, TEXT("%s"), *tempString);
	if (bLogToScreen)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Cyan, tempString);
	}
}

void APhysicsTestActorBase::LogInfo(const int32 machineID, const FVector curLoc, const FDateTime curTime)
{
	// this log function is only supposed to be called by server when data is synchronized across server
	if (!bDoLogPosition)
	{
		return;
	}
	FString tempString = 
		"[" + (machineID != SERVER_ID ? "client " + FString::FromInt(machineID) : "server") + 
		"]Position of: " + UKismetSystemLibrary::GetDisplayName(this) + " is: " + curLoc.ToString() +
		"   ---   " + curTime.ToString() + "." + FString::FromInt(curTime.GetMillisecond());
	UE_LOG(LogTemp, Log, TEXT("%s"), *tempString);
}

void APhysicsTestActorBase::DrawDebugInfo()
{
	if (localMachineID < 0 || localMachineID >= clientDebugColors.Num())
	{
		return;
	}
	const FVector curLoc = GetActorLocation();
	// draw debug point and line
	DrawHelper(prevLocalPosition, curLoc, clientDebugColors[localMachineID], bDrawDebugLine);
	prevLocalPosition = curLoc;
}

void APhysicsTestActorBase::ExchangeLocationInfo()
{
	if (!bDoLogPosition || !bIsSimulationOn)
	{
		return;
	}
	if (HasAuthority())
	{
		ServerExchangeLocationInfo(SERVER_ID, GetActorLocation(), FDateTime::UtcNow());
	}
	else
	{
		UWorld* world = GetWorld();
		if (!world)
		{
			return;
		}
		ADPPlayerController* tempPlayer = Cast<ADPPlayerController>(world->GetFirstPlayerController());
		if (!tempPlayer)
		{
			return;
		}
		tempPlayer->ServerSyncDPActorLocationInfo(this, localMachineID, GetActorLocation(), FDateTime::UtcNow());
		// only for non-server machines we draw our own machine location
		DrawDebugInfo();
	}
}

void APhysicsTestActorBase::DrawHelper(const FVector prevLoc, const FVector curLoc, const FColor drawColor, bool bDoDrawLine)
{
	UWorld* world = GetWorld();
	// draw debug point and line
	DrawDebugPoint(world, curLoc, debugPointSize, drawColor, false, debugGraphicLife);
	if (bDoDrawLine)
	{
		DrawDebugLine(world, prevLoc, curLoc, drawColor, false, debugGraphicLife);
	}
}

void APhysicsTestActorBase::EnableSimulation()
{
	LocalActivateSimulation();
	UWorld* world = GetWorld();
	world->GetTimerManager().ClearTimer(simulationDelayTimer);
}

void APhysicsTestActorBase::OnRep_EnableSimulation()
{
	AssignObjectClientID();
	MulticastStartSimulation(bIsSimulationOn);
}

void APhysicsTestActorBase::AssignObjectClientID()
{
	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}
	ADPPlayerController* tempPlayer = Cast<ADPPlayerController>(world->GetFirstPlayerController());
	if (!tempPlayer)
	{
		return;
	}
	localMachineID = tempPlayer->clientID;
	UE_LOG(LogTemp, Log, TEXT("Client number is: %d"), tempPlayer->clientID);
	machineIDText = "Client " + FString::FromInt(tempPlayer->clientID);
}

void APhysicsTestActorBase::SyncMovement()
{
	// Send location information to remote machine
	if (netOwnerID == localMachineID)
	{
		ServerSyncMovement(GetActorLocation(), GetStaticMeshComponent()->GetPhysicsLinearVelocity(), FDateTime::UtcNow());
	}
}

void APhysicsTestActorBase::MulticastExchangeLocationInfo_Implementation(const int32 machineID, const FVector curLoc)
{
	if (!bIsSimulationOn)
	{
		return;
	}
	// this should not be executed on the server
	if (HasAuthority())
	{
		return;
	}
	// TODO probably should draw together using the multi cast
	// this new debug info does not need to be drawn if it is of the local machine's
	if (machineID == localMachineID)
	{
		return;
	}
	if (machinesLocationInfo.Num() < assumedClientNumber + 1)
	{
		UWorld* world = GetWorld();
		if (!world)
		{
			return;
		}
		ADPPlayerController* tempPlayer = Cast<ADPPlayerController>(world->GetFirstPlayerController());
		if (!tempPlayer)
		{
			return;
		}
		const int32 tempID = tempPlayer->clientID;
		UE_LOG(LogTemp, Warning, TEXT("Object %s not yet initialized on client %d"), *(UKismetSystemLibrary::GetDisplayName(this)), tempID);
		return;
	}
	// when its server, treat it different
	if (machineID == SERVER_ID)
	{
		// always store server info into the last index
		bool bDoDrawLine = bDrawDebugLine;
		if (machinesLocationInfo[assumedClientNumber].machinID != SERVER_ID)
		{
			machinesLocationInfo[assumedClientNumber].machinID = SERVER_ID;
			bDoDrawLine = false;
		}
		DrawHelper(machinesLocationInfo[assumedClientNumber].position, curLoc, serverDebugColor, bDoDrawLine);
		machinesLocationInfo[assumedClientNumber].position = curLoc;
		return;
	}
	if (machineID >= machinesLocationInfo.Num() || machineID < 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("machine id: %d is not valid"), machineID);
		return;
	}
	// skip line drawing if previously no point is communicated to this machine
	if (machinesLocationInfo[machineID].machinID < 0)
	{
		machinesLocationInfo[machineID].machinID = machineID;
		DrawHelper(machinesLocationInfo[machineID].position, curLoc, clientDebugColors[machineID], false);
		machinesLocationInfo[machineID].position = curLoc;
		return;
	}
	// under common situation draw line and point for debugging
	DrawHelper(machinesLocationInfo[machineID].position, curLoc, clientDebugColors[machineID], bDrawDebugLine);
	machinesLocationInfo[machineID].position = curLoc;
}

void APhysicsTestActorBase::ServerExchangeLocationInfo_Implementation(const int32 machineID, const FVector curLoc, const FDateTime curTime)
{
	if (!bDoPresentServer && machineID == SERVER_ID)
	{
		return;
	}
	// when simulation is decided to stop on the server, stop accepting any data
	if (!bIsSimulationOn)
	{
		return;
	}
	MulticastExchangeLocationInfo(machineID, curLoc);
	LogInfo(machineID, curLoc, curTime);
	// store the information into curve lists
	if (machineID == SERVER_ID)
	{
		machinesPointSets[assumedClientNumber].points.Add(FTimeStampLocationInfo(curTime, curLoc));
		return;
	}
	if (machineID < 0 || machineID > machinesPointSets.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Invalid machine ID on server call at %d"), machineID);
		return;
	}
	machinesPointSets[machineID].points.Add(FTimeStampLocationInfo(curTime, curLoc));
}

bool APhysicsTestActorBase::ServerExchangeLocationInfo_Validate(const int32 machineID, const FVector curLoc, const FDateTime curTime)
{
	return true;
}
