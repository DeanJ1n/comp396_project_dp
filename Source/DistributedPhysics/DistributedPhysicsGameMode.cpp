// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DistributedPhysicsGameMode.h"
#include "DistributedPhysicsCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "DPPlayerController.h"
#include "Engine/World.h"
#include "PhysicsTestActorBase.h"
#include "EngineUtils.h"

ADistributedPhysicsGameMode::ADistributedPhysicsGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ADistributedPhysicsGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	ADPPlayerController* tempPlayer = Cast<ADPPlayerController>(NewPlayer);
	if (!tempPlayer)
	{
		return;
	}
	tempPlayer->clientID = ++clientIDCounter;
	// assign object net owner after any post login
	UWorld* world = GetWorld();
	TArray<APhysicsTestActorBase*> actors;
	for (TActorIterator<APhysicsTestActorBase> it(world, APhysicsTestActorBase::StaticClass()); it; ++it)
	{
		actors.Add(*it);
	}
	TArray<ADPPlayerController*> players;
	for (TActorIterator<ADPPlayerController> it(world, ADPPlayerController::StaticClass()); it; ++it)
	{
		players.Add(*it);
	}
	// assign net owner to objects with respect to current objects
	for (int32 i = 0; i < actors.Num(); i++)
	{
		actors[i]->AssignNetOwner(players[i % players.Num()]);
	}
}

void ADistributedPhysicsGameMode::BeginPlay()
{
	Super::BeginPlay();

}
