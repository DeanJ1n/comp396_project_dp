// Fill out your copyright notice in the Description page of Project Settings.


#include "PreCollisionConvergeActor.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"

APreCollisionConvergeActor::APreCollisionConvergeActor()
{
	UStaticMeshComponent* mesh = GetStaticMeshComponent();
	if (mesh)
	{
		mesh->OnComponentHit.AddDynamic(this, &APreCollisionConvergeActor::OnHit);
	}
}

void APreCollisionConvergeActor::LocalActivateSimulation(const bool bDoActivate /* = true */)
{
	Super::LocalActivateSimulation(bDoActivate);
	if (bDoActivate && netOwnerID == localMachineID)
	{
		// activate syncing of movement
		UWorld* world = GetWorld();
		if (!world)
		{
			return;
		}
		world->GetTimerManager().SetTimer(movementSyncTimer, this, &APreCollisionConvergeActor::SyncMovement, movementSyncPeriod, true);
	}
}

void APreCollisionConvergeActor::TickConvergence(float deltaTime)
{
	Super::TickConvergence(deltaTime);
	if (bIsMovementSyncingOn && (netOwnerID != localMachineID) && !bIsAfterHit)
	{
		FVector intendedLocation = FMath::VInterpTo(GetActorLocation(), targetLocation, deltaTime, interpolationSpeed);
		SetActorLocation(intendedLocation, false, nullptr, ETeleportType::TeleportPhysics);
		// for remote machine, interpolate location and velocity
		UStaticMeshComponent* comp = GetStaticMeshComponent();
		if (!comp)
		{
			return;
		}
		FVector intendedVelocity = FMath::VInterpTo(comp->GetPhysicsLinearVelocity(), targetVelocity, deltaTime, interpolationSpeed);
		comp->SetPhysicsLinearVelocity(intendedVelocity);
	}
}

void APreCollisionConvergeActor::SyncMovement()
{
	Super::SyncMovement();
}

void APreCollisionConvergeActor::OnHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	// change the behavior of the actor only as it collide with another DP actor
	if (Cast<APhysicsTestActorBase>(otherActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("After hit, convergence will terminate soon"));
		// TODO a smoother set might be required
		bIsAfterHit = true;
	}
}
