// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PhysicsTestActorBase.h"
#include "ConstantConvergeActor.generated.h"

/**
 * 
 */
UCLASS()
class DISTRIBUTEDPHYSICS_API AConstantConvergeActor : public APhysicsTestActorBase
{
	GENERATED_BODY()

public:
	AConstantConvergeActor();

	virtual void LocalActivateSimulation(const bool bDoActivate /* = true */);
	virtual void TickConvergence(float deltaTime);
	virtual void SyncMovement();
	
};
