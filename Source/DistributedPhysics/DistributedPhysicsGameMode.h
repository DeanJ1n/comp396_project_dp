// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DistributedPhysicsGameMode.generated.h"

UCLASS(minimalapi)
class ADistributedPhysicsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADistributedPhysicsGameMode();
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Information Viewing")
	bool bIsRewindMode = false;

protected:
	UPROPERTY(BlueprintReadWrite, Category = "Client Management")
	int32 clientIDCounter = -1;

};



