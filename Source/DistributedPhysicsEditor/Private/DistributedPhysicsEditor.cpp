#include "DistributedPhysicsEditor.h"
#include "LevelEditor.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"
#include "DistributedPhysicsEditorStyle.h"
#include "DistributedPhysicsEditorCommands.h"
#include "MainFrame.h"
#include "HAL/PlatformFilemanager.h"
#include "HAL/PlatformFile.h"
#include "HAL/FileManagerGeneric.h"
#include "Misc/FileHelper.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Input/SSpinBox.h"
#include "Widgets/Text/STextBlock.h"
#include "Editor/EditorEngine.h"
#include "TimerManager.h"
#include "EngineAnalytics.h"
#include "Interfaces/IAnalyticsProvider.h"
#include "IAssetViewport.h"

IMPLEMENT_GAME_MODULE(FDistributedPhysicsEditorModule, DistributedPhysicsEditor);
#define LOCTEXT_NAMESPACE "FDistributedPhysicsEditorModule"

const float SUMMARY_DELAY = 2.0f;
const FString SUFFIX = ".csv";

void FDistributedPhysicsEditorModule::StartupModule()
{
	// initialize editor module style
	FDistributedPhysicsEditorStyle::Initialize();
	FDistributedPhysicsEditorStyle::ReloadTextures();

	// register commands
	FDistributedPhysicsEditorCommands::Register();
	moduleCommands = MakeShareable(new FUICommandList);
	moduleCommands->MapAction(
		FDistributedPhysicsEditorCommands::Get().AutomationTest,
		FExecuteAction::CreateRaw(this, &FDistributedPhysicsEditorModule::AutoTest),
		FCanExecuteAction()
	);

    UE_LOG(LogTemp, Log, TEXT("Distributed Physics Editor Module Loaded"));

	// show the button on tool bar
	FLevelEditorModule& levelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
	TSharedPtr<FExtender> toolBarExtender = MakeShareable(new FExtender);
	toolBarExtender->AddToolBarExtension("Game", EExtensionHook::After, moduleCommands, FToolBarExtensionDelegate::CreateRaw(this, &FDistributedPhysicsEditorModule::AddToolbarExtension));
	levelEditorModule.GetToolBarExtensibilityManager()->AddExtender(toolBarExtender);
}

void FDistributedPhysicsEditorModule::ShutdownModule()
{
	// clear timer handles
	GEditor->GetTimerManager()->ClearTimer(autoTestTimerHandle);
	// shut down properly
	FDistributedPhysicsEditorStyle::Shutdown();
	FDistributedPhysicsEditorCommands::Unregister();
    UE_LOG(LogTemp, Log, TEXT("Distributed Physics Editor Module Shutdown"));
}

void FDistributedPhysicsEditorModule::AutoTest()
{
	// first to display a page for selecting test
	FAutoTestSettings temp;
	temp.interactionWindow = 
		SNew(SWindow)
		.Title(LOCTEXT("AutoTestSettings", "Auto Test"))
		.SupportsMaximize(false).SupportsMinimize(false)
		.SizingRule(ESizingRule::Autosized)
		.FocusWhenFirstShown(true)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(STextBlock)
				.Text(LOCTEXT("AutoTestingNum", "Tests to perform: "))
			]
			
			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(SSpinBox<int32>)
				.MinSliderValue(0)
				.MaxSliderValue(1000)
				.MinValue(0)
				.MaxValue(1000)
				.OnValueChanged_Raw(&temp, &FAutoTestSettings::OnTestNumberChanged)
				.Value(temp.testNum)
			]

			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(STextBlock)
				.Text(LOCTEXT("SingleTestDuration", "Duration for each test: "))
			]

			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(SSpinBox<float>)
				.MinSliderValue(0)
				.MaxSliderValue(100)
				.MinValue(0)
				.MaxValue(100)
				.OnValueChanged_Raw(&temp, &FAutoTestSettings::OnSingleTestDurationChanged)
				.Value(temp.singleDuration)
			]

			+ SVerticalBox::Slot()
			.AutoHeight()
			[
				SNew(SButton)
				.OnClicked_Raw(&temp, &FAutoTestSettings::OnConfirmationClicked)
				[
					SNew(STextBlock)
					.Text(LOCTEXT("ConfirmButtonTips", "Start Tests"))
				]
			]
		];
	IMainFrameModule& mainFrame = FModuleManager::LoadModuleChecked<IMainFrameModule>("MainFrame");
	TSharedPtr<SWindow> parentWindow = mainFrame.GetParentWindow();
	// bring up the window
	if (parentWindow.IsValid())
	{
		FSlateApplication::Get().AddModalWindow(temp.interactionWindow.ToSharedRef(), parentWindow.ToSharedRef());
	}
	else
	{
		FSlateApplication::Get().AddWindow(temp.interactionWindow.ToSharedRef());
	}

	// use info gathered from above interaction window to conduct tests
	if (temp.bDoAutoTest)
	{
		UE_LOG(LogTemp, Warning, TEXT("Auto test starting..."));
		// initiate files for recording test results
		IPlatformFile& platFiles = FPlatformFileManager::Get().GetPlatformFile();
		FString autoTestDirPath = FPaths::ProjectDir() + "AutoTests/";
		if (!platFiles.DirectoryExists(*autoTestDirPath))
		{
			UE_LOG(LogTemp, Warning, TEXT("Cannot find directory: %s, creating one for auto testing"), *autoTestDirPath);
			platFiles.CreateDirectory(*autoTestDirPath);
		}
		// create a test indicator
		IFileHandle* writeHandle = platFiles.OpenWrite(*(autoTestDirPath + "indicator.txt"));
		if (writeHandle)
		{
			writeHandle->Flush();
			delete writeHandle;
		}
		ConductTests(0, temp.testNum, temp.singleDuration);
	}
}

// just a helper function for launching PIE copied from engine source code at DebuggerCommands.cpp
void RecordLastExecutedPlayMode()
{
	if (FEngineAnalytics::IsAvailable())
	{
		const ULevelEditorPlaySettings* PlaySettings = GetDefault<ULevelEditorPlaySettings>();

		// play location
		FString PlayLocationString;

		switch (PlaySettings->LastExecutedPlayModeLocation)
		{
		case PlayLocation_CurrentCameraLocation:
			PlayLocationString = TEXT("CurrentCameraLocation");
			break;

		case PlayLocation_DefaultPlayerStart:
			PlayLocationString = TEXT("DefaultPlayerStart");
			break;

		default:
			PlayLocationString = TEXT("<UNKNOWN>");
		}

		// play mode
		FString PlayModeString;

		switch (PlaySettings->LastExecutedPlayModeType)
		{
		case PlayMode_InViewPort:
			PlayModeString = TEXT("InViewPort");
			break;

		case PlayMode_InEditorFloating:
			PlayModeString = TEXT("InEditorFloating");
			break;

		case PlayMode_InMobilePreview:
			PlayModeString = TEXT("InMobilePreview");
			break;

		case PlayMode_InTargetedMobilePreview:
			PlayModeString = TEXT("InTargetedMobilePreview");
			break;

		case PlayMode_InVulkanPreview:
			PlayModeString = TEXT("InVulkanPreview");
			break;

		case PlayMode_InNewProcess:
			PlayModeString = TEXT("InNewProcess");
			break;

		case PlayMode_InVR:
			PlayModeString = TEXT("InVR");
			break;

		case PlayMode_Simulate:
			PlayModeString = TEXT("Simulate");
			break;

		default:
			PlayModeString = TEXT("<UNKNOWN>");
		}

		FEngineAnalytics::GetProvider().RecordEvent(TEXT("Editor.Usage.PIE"), TEXT("PlayLocation"), PlayLocationString);
		FEngineAnalytics::GetProvider().RecordEvent(TEXT("Editor.Usage.PIE"), TEXT("PlayMode"), PlayModeString);
	}
}

void FDistributedPhysicsEditorModule::ConductTests(const int32 curTime, const int32 totalNum, const float duration)
{
	// Set last executed play mode
	ULevelEditorPlaySettings* playSettings = GetMutableDefault<ULevelEditorPlaySettings>();
	playSettings->LastExecutedPlayModeType = EPlayModeType::PlayMode_InEditorFloating;
	FPropertyChangedEvent propChangeEvent(ULevelEditorPlaySettings::StaticClass()->FindPropertyByName(GET_MEMBER_NAME_CHECKED(ULevelEditorPlaySettings, LastExecutedPlayModeType)));
	playSettings->PostEditChangeProperty(propChangeEvent);
	playSettings->SaveConfig();

	// First stop any currently playing in editor
	// TODO possible pause between termination and lauching might be required
	if (GEditor->PlayWorld)
	{
		GUnrealEd->RequestEndPlayMap();
	}

	// depending on time of tests conducted, we conduct another test or just exit this function
	if (curTime >= totalNum)
	{
		UE_LOG(LogTemp, Warning, TEXT("Auto test terminated"));
		IPlatformFile& platFiles = FPlatformFileManager::Get().GetPlatformFile();
		const FString autoTestDirPath = FPaths::ProjectDir() + "AutoTests/";
		if (platFiles.FileExists(*(autoTestDirPath + "indicator.txt")))
		{
			platFiles.DeleteFile(*(autoTestDirPath + "indicator.txt"));
		}
		// delay and do summary
		FTimerDelegate summaryDelegate = FTimerDelegate::CreateRaw(this, &FDistributedPhysicsEditorModule::PresentSummary);
		GEditor->GetTimerManager()->SetTimer(autoTestTimerHandle, summaryDelegate, SUMMARY_DELAY, false);
		return;
	}
	FLevelEditorModule& levelEditorModule = FModuleManager::GetModuleChecked<FLevelEditorModule>(TEXT("LevelEditor"));
	RecordLastExecutedPlayMode();
	// decide player location
	const bool bAtStartLocation =
		(GEditor->CheckForPlayerStart()
			? static_cast<EPlayModeLocations>(GetDefault<ULevelEditorPlaySettings>()->LastExecutedPlayModeLocation)
			: PlayLocation_CurrentCameraLocation)
		== EPlayModeLocations::PlayLocation_DefaultPlayerStart;
	const FVector* startLoc = NULL;
	const FRotator* startRot = NULL;
	// when not at player start
	if (!bAtStartLocation)
	{
		TSharedPtr<IAssetViewport> activeLevelViewport = levelEditorModule.GetFirstActiveViewport();
		if (activeLevelViewport.IsValid() &&
			FSlateApplication::Get().FindWidgetWindow(activeLevelViewport->AsWidget()).IsValid())
		{
			// Start the player where the camera is if not forcing from player start
			startLoc = &activeLevelViewport->GetAssetViewportClient().GetViewLocation();
			startRot = &activeLevelViewport->GetAssetViewportClient().GetViewRotation();
		}
	}
	// lauch PIE
	GUnrealEd->RequestPlaySession(bAtStartLocation, NULL, false, startLoc, startRot);

	UE_LOG(LogTemp, Warning, TEXT("Conducting test #%d, out of a total of %d tests"), curTime + 1, totalNum);

	// set timer for conducting another test after this one
	FTimerDelegate testDelegate = FTimerDelegate::CreateRaw(this, &FDistributedPhysicsEditorModule::ConductTests, curTime + 1, totalNum, duration);
	GEditor->GetTimerManager()->SetTimer(autoTestTimerHandle, testDelegate, duration, false);
}

void FDistributedPhysicsEditorModule::PresentSummary()
{
	// find the corresponding directory as test map
	const FString mapName = GCurrentLevelEditingViewportClient->GetWorld()->GetName();
	IPlatformFile& platFiles = FPlatformFileManager::Get().GetPlatformFile();
	const FString autoTestDirPath = FPaths::ProjectDir() + "AutoTests/";
	const FString autoTestMapDir = autoTestDirPath + mapName + "/";
	if (!platFiles.DirectoryExists(*autoTestMapDir))
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot find map auto test directory: %s"), *autoTestMapDir);
		return;
	}
	// compute all summary for all files
	TArray<FString> fileNames;
	FFileManagerGeneric::Get().FindFiles(fileNames, *autoTestMapDir, *SUFFIX);
	// summary for each file
	for (const FString temp : fileNames)
	{
		const FString fullFileName = autoTestMapDir + temp;
		FString info = "";
		FFileHelper::LoadFileToString(info, *fullFileName);
		PresentSummaryIndividual(info, temp);
	}
}

void FDistributedPhysicsEditorModule::PresentSummaryIndividual(const FString info, const FString name)
{

}

void FDistributedPhysicsEditorModule::AddToolbarExtension(FToolBarBuilder& builder)
{
	// extend level toolbar menu
	builder.BeginSection(TEXT("Auto Test"));
	builder.AddToolBarButton(FDistributedPhysicsEditorCommands::Get().AutomationTest);
	builder.EndSection();
}

void FAutoTestSettings::OnTestNumberChanged(int32 num)
{
	testNum = num;
}

void FAutoTestSettings::OnSingleTestDurationChanged(float dur)
{
	singleDuration = dur;
}

FReply FAutoTestSettings::OnConfirmationClicked()
{
	if (!interactionWindow.IsValid())
	{
		return FReply::Unhandled();
	}
	// when clicked on confirm button, do auto testing
	bDoAutoTest = true;
	FSlateApplication::Get().RequestDestroyWindow(interactionWindow.ToSharedRef());
	return FReply::Handled();
}

#undef LOCTEXT_NAMESPACE
