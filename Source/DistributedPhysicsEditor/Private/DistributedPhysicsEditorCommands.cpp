#include "DistributedPhysicsEditorCommands.h"

#define LOCTEXT_NAMESPACE "FDistributedPhysicsEditorModule"

void FDistributedPhysicsEditorCommands::RegisterCommands()
{
	// register commands we need
	UI_COMMAND(AutomationTest, "AutoTest", "Test current scene automatically with a summary at the end", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
