#include "DistributedPhysicsEditorStyle.h"
#include "DistributedPhysicsEditor.h"
#include "Framework/Application/SlateApplication.h"
#include "Styling/SlateStyleRegistry.h"
#include "Slate/SlateGameResources.h"
#include "Interfaces/IPluginManager.h"

TSharedPtr< FSlateStyleSet > FDistributedPhysicsEditorStyle::StyleInstance = NULL;

void FDistributedPhysicsEditorStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FDistributedPhysicsEditorStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

FName FDistributedPhysicsEditorStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("DistributedPhysicsEditorStyle"));
	return StyleSetName;
}

#define IMAGE_BRUSH( RelativePath, ... ) FSlateImageBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )

const FVector2D Icon16x16(16.0f, 16.0f);
const FVector2D Icon20x20(20.0f, 20.0f);
const FVector2D Icon40x40(40.0f, 40.0f);

TSharedRef< FSlateStyleSet > FDistributedPhysicsEditorStyle::Create()
{
	TSharedRef< FSlateStyleSet > Style = MakeShareable(new FSlateStyleSet("DistributedPhysicsEditorStyle"));
	Style->SetContentRoot(FPaths::ProjectDir() + "Resources/");

	Style->Set("DistributedPhysicsEditor.AutoTest", new IMAGE_BRUSH(TEXT("AutomationTest"), Icon40x40));

	return Style;
}

#undef IMAGE_BRUSH

void FDistributedPhysicsEditorStyle::ReloadTextures()
{
	if (FSlateApplication::IsInitialized())
	{
		FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
	}
}

const ISlateStyle& FDistributedPhysicsEditorStyle::Get()
{
	return *StyleInstance;
}

