
using UnrealBuildTool;

public class DistributedPhysicsEditor : ModuleRules
{
	public DistributedPhysicsEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(
            new string[]
            {
                "DistributedPhysicsEditor/Public"
            });

        PrivateIncludePaths.AddRange(
            new string[] 
            {
		        "DistributedPhysicsEditor/Private"
	        });
		PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "CoreUObject",
                "Engine",
                "InputCore",
                "Slate",
                "SlateCore",
                "DistributedPhysics",
                "UnrealEd",
                "EditorStyle",
                "LevelEditor",
                "MainFrame",
                "Analytics"
            });
    }
}