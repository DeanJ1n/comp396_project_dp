#pragma once

#include "Engine.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"
#include "UnrealEd.h"

class FReply;
struct FTimerHandle;

DECLARE_LOG_CATEGORY_EXTERN(DistributedPhysicsEditor, All, All)

class FDistributedPhysicsEditorModule: public IModuleInterface
{
public:
    virtual void StartupModule() override;
    virtual void ShutdownModule() override;

	void AutoTest();
	void ConductTests(const int32 curTime, const int32 totalNum, const float duration);
	void PresentSummary();
	void PresentSummaryIndividual(const FString info, const FString name);

private:
	// extend the toll bar
	void AddToolbarExtension(FToolBarBuilder& builder);

private:
	TSharedPtr<class FUICommandList> moduleCommands;
	FTimerHandle autoTestTimerHandle;

};

// a struct to hold the infromation regarding auto testing set up
struct FAutoTestSettings
{
public:
	int32 testNum = 0;	// nmber of tests to conduct
	float singleDuration = 30.0f;	// time for each individual test to last
	bool bDoAutoTest = false;	// whether the user canceled the test or not
	TSharedPtr<SWindow> interactionWindow;	// window to interact with

public:
	void OnTestNumberChanged(int32 num);
	void OnSingleTestDurationChanged(float dur);
	FReply OnConfirmationClicked();

};
