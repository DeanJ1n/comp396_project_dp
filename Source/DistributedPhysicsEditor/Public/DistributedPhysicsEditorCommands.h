#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "DistributedPhysicsEditorStyle.h"

class FDistributedPhysicsEditorCommands : public TCommands<FDistributedPhysicsEditorCommands>
{
public:
	FDistributedPhysicsEditorCommands() : 
		TCommands<FDistributedPhysicsEditorCommands>(
			TEXT("DistributedPhysicsEditor"),
			NSLOCTEXT("Contexts", "DistributedPhysicsEditor", "DistributedPhysicsEditor Utilities"),
			NAME_None,
			FDistributedPhysicsEditorStyle::GetStyleSetName()){}
	virtual void RegisterCommands() override;

	// commands for editor utilities
	TSharedPtr< FUICommandInfo > AutomationTest;
};
